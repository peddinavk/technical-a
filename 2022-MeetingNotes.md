# Meeting Agenda 13/12/2022

- Guest Speaker Sebastian Baer, about [openfasttrace](https://github.com/itsallcode/openfasttrace) 
- Architecture presentation follow up, a CNFC style map - Daniel L. 
- Working with other communities (SOAFEE, COVESA, etc..) - Daniel K.
- Review of Facilitators for the breakout groups
    - [Unified in-vehicle software orchestration](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/vehicle-sw-orchestration-sdv-topic/-/wikis/home)
    - [Observability and monitoring](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/observability-monitoring-sdv-topic/-/wikis/home)
    - [Testing and Validation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/testing-validation-sdv-topic/-/wikis/home)
    - [Confidential Compute and Multi-tenancy](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/confidential-compute-sdv-topic/-/wikis/home)
    - [“SDV-style” Programming Model](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/programming-model-sdv-topic/-/wikis/home)
    - [Resource- and Capability-aware Scheduling](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/scheduling-sdv-topic/-/wikis/home)
- Migration to Gitlab: [meeting notes archive](https://docs.google.com/document/d/16bZ7WpuoxFHCdftrF2-vt7IZchKMKITQSOZFfOZbNok/edit?usp=sharing)


