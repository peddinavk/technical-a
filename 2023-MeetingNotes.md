# Tech Alignment Call - Meeting Notes 2023

# Meeting Agenda 07/02/2023 
- Daniel K: first thoughts on "what might 'automotive-grade' mean for us?"
- Jonas Wolf: show&tell gsn2x Project (https://github.com/jonasthewolf/gsn2x)

# Meeting Agenda 24/01/2023 
- Thomas S [presentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/technical-a/-/blob/main/CES%20SdV%20Projects%20Overview.pptx) held at CES
- CES summary and impressions 
- Breakout group reminder: kickoff meetings starting next week
- merging the topics: scheduling-sdv-topic and sw orchestration 
- Use case/scenario collection for Community Days event
- Community Days [website](https://sdv.eclipse.org/sdv-community-day-lisbon-2023/) and [registration](https://www.eventbrite.com/e/sdv-community-day-lisbon-march-2023-tickets-520519135747) online, [agenda](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-days/2023-q1-community-days/-/wikis/home)
- open discussion 

## Use cases so far 
- truck fleet management: where trucks run SDV stacks so that logistics fleet operators can manage a diverse set of vehicles by having their fleet operator service supplier install data collection clients and other related features into the trucks, as 3rd party SDV-style applications.
- dynamic car insurance: where insurance companies offer rates that dynamically adapt to driving behavior, implemented via 3rd party SDV-style data collection/evaluation applications that run on SDV stacks in customer vehicles. 

## Meeting notes 
- Decision: OK to merge the two breakout groups: scheduling and SW orchestration


# Meeting Agenda 09/01/2023 
- Recap on initiatives & landscape vision 
- Thomas S presentation held at CES - *moved to next meeting*
- CES summary and impressions - *moved to next meeting*

## Meeting notes 
### Other SDV related initiatives - What is the landscape around EF SDV

Recap of other initiatives open source or not, that are related to EF SDV.
SOAFEE and Eclipse SDV are orthogonal initiatives, but some EF projects such as Leda and Chariott are bridging the gap. 
Kuksa and Velocitas are implementing COVESA VSS. Velocitas is also using the digital.auto platform. 

![MapAutoSWInitiatives](./MapAutoSWInitiatives.png)

![SOAFEEandEFSDV](./SOAFEEandEFSDV.png)

![CovesaAndAutosar.png](./CovesaAndAutosar.png)

## Review of the break out group topics

Deadline for signing up it this week. An [overview](https://gitlab.eclipse.org/groups/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/-/wikis/home) of the topics is to be found here. 

## Review of the SDV landscape

*Call to action*: project participants please help us sort your projects in the correct box! 
Topic area to explore is the definition of requirements, a starting point could the insurance companies requirements (https://openinsurance.io/). This topic could be explored in the ["Testing and Validation"](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/testing-validation-sdv-topic/-/wikis/home) group. 


![Landscape.png](./Landscape.png)


